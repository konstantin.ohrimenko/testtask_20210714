<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Seeders\ActiveSubstancesTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ActiveSubstanceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test for active substance
     *
     * @return void
     */
    public function test_active_substance()
    {
        $this->seed(ActiveSubstancesTableSeeder::class);
        $this->test_unauthenteficated();
        $this->test_authenteficated();
    }

    /**
     * Test for authenteficated user.
     *
     * @return void
     */
    private function test_authenteficated()
    {
        $user = User::factory()->create();
        $token = JWTAuth::fromUser($user);

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
                ->get('/api/entity/active-substance')
                ->assertJsonStructure(['*' => ['id', 'name']]);
        
        $response->assertStatus(200);
    }

    /**
     * Test for not authenteficated user.
     *
     * @return void
     */
    private function test_unauthenteficated()
    {
        // No Authorization header
        $response = $this->get('/api/entity/active-substance');
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->get('/api/entity/active-substance');
        $response->assertStatus(401);
    }
}
