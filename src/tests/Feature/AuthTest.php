<?php

namespace Tests\Feature;

use Database\Seeders\UsersTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    
    private $failLoginList = [
        [],
        [
            'email' => 'fail@example.com',
            'password' => 'fail',
        ],
        [
            'email' => 'demo@example.com',
            'password' => '',
        ],
        [
            'email' => '',
            'password' => 'demo',
        ],
        [
            'login' => '',
            'password' => 'demo',
        ],
    ];
    
    /**
     * Test fail login
     *
     * @return void
     */
    public function test_login_fail()
    {
        $this->seed(UsersTableSeeder::class);

        foreach ($this->failLoginList as $body) {
            $response = $this->post('/api/auth/login', $body);
            $response->assertStatus(401);
        }
    }
    
    /**
     * Test success login
     *
     * @return void
     */
    public function test_login_success()
    {
        $this->seed(UsersTableSeeder::class);

        $body = [
            'email' => 'demo@example.com',
            'password' => 'demo',
        ];
        $response = $this->post('/api/auth/login', $body);
        $response->assertStatus(200)
                ->assertJsonStructure([
                    'token', 'token_type', 'expires_in'
                ]);

    }
}
