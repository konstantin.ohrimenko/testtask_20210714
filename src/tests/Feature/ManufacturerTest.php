<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Seeders\ManufacturersTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ManufacturerTest extends TestCase
{
    /**
     * Test for manufacturer
     *
     * @return void
     */
    public function test_manufacturer()
    {
        $this->seed(ManufacturersTableSeeder::class);
        $this->test_unauthenteficated();
        $this->test_authenteficated();
    }

    /**
     * Test for authenteficated user.
     *
     * @return void
     */
    private function test_authenteficated()
    {
        $user = User::factory()->create();
        $token = JWTAuth::fromUser($user);

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
                ->get('/api/entity/manufacturer')
                ->assertJsonStructure(['*' => ['id', 'name', 'url']]);
        
        $response->assertStatus(200);
    }

    /**
     * Test for not authenteficated user.
     *
     * @return void
     */
    private function test_unauthenteficated()
    {
        // No Authorization header
        $response = $this->get('/api/entity/manufacturer');
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->get('/api/entity/manufacturer');
        $response->assertStatus(401);
    }
}
