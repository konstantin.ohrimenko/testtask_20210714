<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Medicament;
use App\Models\ActiveSubstance;
use App\Models\Manufacturer;
use Database\Seeders\MedicamentsTableSeeder;
use Database\Seeders\ManufacturersTableSeeder;
use Database\Seeders\ActiveSubstancesTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class MedicamentTest extends TestCase
{
    
    private $errorStructure = [
                    'name', 
                    'active_substance_id', 
                    'manufacturer_id', 
                    'price'
                ];
    private $itemStructure = [
                    'id', 
                    'name', 
                    'active_substance_id', 
                    'manufacturer_id', 
                    'price'
                ];
    
    /**
     * Test for medicament
     *
     * @return void
     */
    public function test_manufacturer()
    {
        $this->seed(ManufacturersTableSeeder::class);
        $this->seed(ActiveSubstancesTableSeeder::class);
        $this->seed(MedicamentsTableSeeder::class);
        $this->test_unauthenteficated();
        $this->test_authenteficated();
    }

    /**
     * Test for authenteficated user.
     *
     * @return void
     */
    private function test_authenteficated()
    {
        $user = User::factory()->create();
        $token = JWTAuth::fromUser($user);
        
        $this->testList($token);
        $this->testItem($token);
        $this->testCreate($token);
        
        $medicament = Medicament::factory()->create();
        $this->testUpdate($token, $medicament->id);
        $this->testDelete($token, $medicament->id);
    }
    
    private function testDelete($token, $id)
    {
        $this->testDeleteFail($token);
        $this->testDeleteSuccess($token, $id);
    }
    
    private function testDeleteFail($token)
    {
        $response = $this->makeRequest('delete', $token, '/0');
        $response->assertStatus(404);
    }
    
    private function testDeleteSuccess($token, $id)
    {
        $response = $this->makeRequest('delete', $token, '/' . $id);
        $response->assertStatus(200);
    }
    
    private function testUpdate($token, $id)
    {
        $this->testUpdateFail($token, $id);
        $this->testUpdateSuccess($token, $id);
    }
    
    private function testUpdateFail($token, $id)
    {
        $body = [];
        $response = $this->makeRequest('put', $token, '/' . $id, $body);
        $response->assertJsonStructure($this->errorStructure)
                ->assertStatus(400);
        
        $response = $this->makeRequest('put', $token, '/0', $body);
        $response->assertStatus(404);
    }
    
    private function testUpdateSuccess($token, $id)
    {
        $activeSubstance = ActiveSubstance::factory()->create();
        $manufacturer = Manufacturer::factory()->create();
        $body = [
            'name' => 'test',
            'active_substance_id' => $activeSubstance->id,
            'manufacturer_id' => $manufacturer->id,
            'price' => 1
        ];
        $response = $this->makeRequest('put', $token, '/' . $id, $body);
        $response->assertStatus(200);
    }
    
    private function testCreate($token)
    {
        $this->testCreateFail($token);
        $this->testCreateSuccess($token);
    }
    
    private function testCreateFail($token)
    {
        $body = [];
        $response = $this->makeRequest('post', $token, '', $body);
        $response->assertJsonStructure($this->errorStructure)
                ->assertStatus(400);
    }
    
    private function testCreateSuccess($token)
    {
        $activeSubstance = ActiveSubstance::factory()->create();
        $manufacturer = Manufacturer::factory()->create();
        $body = [
            'name' => 'test',
            'active_substance_id' => $activeSubstance->id,
            'manufacturer_id' => $manufacturer->id,
            'price' => 1
        ];
        $response = $this->makeRequest('post', $token, '', $body);
        $response->assertStatus(200);
    }
    
    private function makeRequest($method, $token, $url = '', $body = [])
    {
        return $this->withHeader('Authorization', 'Bearer ' . $token)
                         ->{$method}('/api/entity/medicament' . $url, $body);
    }

    private function testList($token)
    {
        $this->makeRequest('get', $token)
                ->assertJsonStructure(['*' => $this->itemStructure])
                ->assertStatus(200);
    }

    private function testItem($token)
    {
        $item = Medicament::factory()->create();
        $this->makeRequest('get', $token, '/' . $item->id)
                ->assertJsonStructure($this->itemStructure)
                ->assertStatus(200);
        
        $this->makeRequest('get', $token, '/0')
                ->assertStatus(404);
    }

    /**
     * Test for not authenteficated user.
     *
     * @return void
     */
    private function test_unauthenteficated()
    {
        $this->testNotAuthList();
        $this->testNotAuthItem();
        $this->testNotAuthCreate();
        $this->testNotAuthUpdate();
        $this->testNotAuthDelete();
    }
    
    private function testNotAuthCreate()
    {
        $body = [];
        // No Authorization header
        $response = $this->post('/api/entity/medicament', $body);
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->post('/api/entity/medicament', $body);
        $response->assertStatus(401);
    }
    
    private function testNotAuthUpdate()
    {
        $body = [];
        // No Authorization header
        $response = $this->put('/api/entity/medicament/1', $body);
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->put('/api/entity/medicament/1', $body);
        $response->assertStatus(401);
    }
    
    private function testNotAuthDelete()
    {
        // No Authorization header
        $response = $this->delete('/api/entity/medicament/1');
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->delete('/api/entity/medicament/1');
        $response->assertStatus(401);
    }
    
    private function testNotAuthItem()
    {
        // No Authorization header
        $response = $this->get('/api/entity/medicament/1');
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->get('/api/entity/medicament/1');
        $response->assertStatus(401);
    }
    
    private function testNotAuthList()
    {
        // No Authorization header
        $response = $this->get('/api/entity/medicament');
        $response->assertStatus(401);

        // Wrong Authorization header
        $response = $this->withHeader('Authorization', 'Bearer ')
                         ->get('/api/entity/medicament');
        $response->assertStatus(401);
    }
}
