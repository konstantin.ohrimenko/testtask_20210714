<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function ($router) {
    Route::post('auth/login', 'App\Http\Controllers\AuthController@login');
    
    Route::group(['middleware' => 'auth:api', 'prefix' => 'entity'], function ($router) {
        Route::get('active-substance', 'App\Http\Controllers\ActiveSubstanceController');
        Route::get('manufacturer', 'App\Http\Controllers\ManufacturerController');
        Route::resource('medicament', 'App\Http\Controllers\MedicamentController');
    });

});

Route::fallback(function() {
    return response()->json(['error' => 'Not allowed.'], 403);
});
