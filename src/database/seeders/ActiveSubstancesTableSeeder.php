<?php

namespace Database\Seeders;

use App\Models\ActiveSubstance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActiveSubstancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('active_substances')->truncate();
        ActiveSubstance::factory()
                ->count(100)
                ->create();
    }
}
