<?php

namespace Database\Factories;

use App\Models\ActiveSubstance;
use App\Models\Medicament;
use App\Models\Manufacturer;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicamentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medicament::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'active_substance_id' => ActiveSubstance::pluck('id')->random(),
            'manufacturer_id' => Manufacturer::pluck('id')->random(),
            'price' => $this->faker->randomNumber(2)
        ];
    }
}
