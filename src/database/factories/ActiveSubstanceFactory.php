<?php

namespace Database\Factories;

use App\Models\ActiveSubstance;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActiveSubstanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ActiveSubstance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word()
        ];
    }
}
