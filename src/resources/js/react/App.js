import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import { CookiesProvider } from 'react-cookie';

function App() {
    return (
        <CookiesProvider>
            <Root />
        </CookiesProvider>
    );
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
