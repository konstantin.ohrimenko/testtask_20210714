import React from 'react';
import ReactDOM from 'react-dom';
import Navigation from './Navigation';
import AuthScreen from './screens/Auth';
import { useCookies } from 'react-cookie';

export default function Root() {
    const [cookies, setCookie] = useCookies(['token']);
    if (cookies.token) {
        return <Navigation />;
    }
    return <AuthScreen />;
}
