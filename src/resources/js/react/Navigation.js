import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import HomeScreen from './screens/Home';
import ActiveSubstanceListScreen from './screens/ActiveSubstanceList';
import ManufacturerListScreen from './screens/ManufacturerList';
import MedicamentListScreen from './screens/MedicamentList';
import MedicamentEditScreen from './screens/MedicamentEdit';

//const linking = {
//  prefixes: ['http://localhost:13080', 'https://localhost:13443'],
//  config: {
//    screens: {
//      Auth: '',
//      ActiveSubstances: 'entity/activesubstances',
//      Manufacturers: 'entity/manufacturers',
//      Medicamentes: 'entity/medicamentes',
//      MedicamenteCreate: 'entity/medicamentes/create',
//      MedicamenteEdit: 'entity/medicamentes/:id',
//    }
//  },
//};

export default function Navigation() {
    return (
        <Router>

            <Switch>
                <Route path="/entity/active-substance">
                    <ActiveSubstanceListScreen />
                </Route>
                <Route path="/entity/manufacturer">
                    <ManufacturerListScreen />
                </Route>
                <Route path="/entity/medicament/create">
                    <MedicamentEditScreen />
                </Route>
                <Route path="/entity/medicament/:medicamentId">
                    <MedicamentEditScreen />
                </Route>
                <Route path="/entity/medicament">
                    <MedicamentListScreen />
                </Route>

            { /*
              <Route path="/users">
                <Users />
              </Route>
             */ }
              <Route path="/">
                <HomeScreen />
              </Route>
            </Switch>
        </Router>
    );
}
