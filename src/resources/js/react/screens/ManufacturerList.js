import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {API_URL} from '../const';
import {useCookies} from 'react-cookie';

export default function ManufacturerList() {
    const [itemList, setItemList] = useState([]);
    const [cookies, setCookie] = useCookies(['token']);
    const parseResponse = json => {
        if (Array.isArray(json)) {
            setItemList(json);
        }
    }
    useEffect(() => {
        fetch(API_URL + 'entity/manufacturer', {
            // mode: 'no-cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
                'Authorization': 'Bearer ' + cookies.token,
            },
        })
            .then(response => {
                if (response.status === 401) {
                    setCookie('token', '', { path: '/' });
                    return response.statusText;
                }
                return response.json();
            })
            .then(parseResponse)
            .catch(err => console.log('error', err))
        ;
    }, []);
    const views = itemList.map(item => (
        <tr key={'item-' + item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.url}</td>
        </tr>
    ));
    if (!views.length) {
        views.push(
            <tr key={'item-empty'}>
                <td colSpan={3}>
                    <h4 className="text-center">No data to display</h4>
                </td>
            </tr>
        );
    }
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">Manufacturer list</div>

                        <div className="card-body">
                            <table className="table table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Url</th>
                                </tr>
                                {views}
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
