import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import {API_URL} from '../const';
import { useCookies } from 'react-cookie';

export default function Auth() {
    const [email, setEmail] = useState('demo@example.com');
    const [password, setPassword] = useState('demo');
    const [error, setError] = useState('');
    const [cookies, setCookie] = useCookies(['token']);
    const handleChangeEmail = event => {
        setEmail(event.target.value);
    };
    const handleChangePassword = event => {
        setPassword(event.target.value);
    };
    const submit = () => {
        setError('');
        const body = new FormData();
        body.set('email', email);
        body.set('password', password);
        fetch(API_URL + 'auth/login', {
            method: 'POST',
            body,
        })
            .then(result => result.json())
            .then(json => {
                if (json.error) {
                    setError(json.error);
                } else {
                    setCookie('token', json.token, { path: '/' });
                }
            })
            .catch(err => {
                setError('Unknown error!');
            });
    };
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Login</div>

                        <div className="card-body">
                            {error ? <div className="alert alert-danger">{error}</div> : null}
                            <div className="form-group row">
                                <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Login</label>

                                <div className="col-md-6">
                                    <input id="email" className="form-control" value={email} required autoFocus onChange={handleChangeEmail} />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label htmlFor="password" className="col-md-4 col-form-label text-md-right">Password</label>

                                <div className="col-md-6">
                                    <input id="password" type="password"
                                           className="form-control"
                                           value={password}
                                           onChange={handleChangePassword}
                                           required />
                                </div>
                            </div>

                            <div className="form-group row mb-0">
                                <div className="col-md-8 offset-md-4">
                                    <button onClick={submit} type="button" className="btn btn-primary">Submit</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
