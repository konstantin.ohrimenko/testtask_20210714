import React from 'react';
import ReactDOM from 'react-dom';

export default function SplashScreen() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Error</div>

                        <div className="card-body">Page not found</div>
                    </div>
                </div>
            </div>
        </div>
            );
}