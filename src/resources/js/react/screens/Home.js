import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';

export default function Home() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">Home</div>

                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <Link to="/entity/active-substance">Active substance list</Link>
                                </div>
                                <div className="col-12 col-md-4">
                                    <Link to="/entity/manufacturer">Manufacturer list</Link>
                                </div>
                                <div className="col-12 col-md-4">
                                    <Link to="/entity/medicament">Medicament list</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
