import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {API_URL} from '../const';
import {useCookies} from 'react-cookie';
import {
    useParams,
    useHistory
} from "react-router-dom";

export default function MedicamentEdit() {
    const [item, setItem] = useState({
        id: 0,
        active_substance_id: 0,
        manufacturer_id: 0,
        name: '',
        price: 0.0,
    });
    const [manufacturers, setManufacturers] = useState([]);
    const [activeSubstances, setActiveSubstances] = useState([]);
    const [errors, setErrors] = useState({});
    const [cookies, setCookie] = useCookies(['token']);
    const { medicamentId } = useParams();
    const history = useHistory();

    const requestData = (route, onSuccess, onError) => {
        fetch(API_URL + route, {
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
                'Authorization': 'Bearer ' + cookies.token,
            },
        })
            .then(response => {
                if (response.status === 401) {
                    setCookie('token', '', { path: '/' });
                    return response.statusText;
                }
                if (response.status === 404) {
                    return response.statusText;
                }
                return response.json();
            })
            .then(onSuccess)
            .catch(onError)
        ;
    };
    const refresh = () => {
        refreshManufacturers();
    };
    const refreshManufacturers = () => {
        requestData('entity/manufacturer', json => {
            if (Array.isArray(json)) {
                setManufacturers(json);
            }
            refreshActiveSubstances();
        }, () => history.goBack());
    };
    const refreshActiveSubstances = () => {
        requestData('entity/active-substance', json => {
            if (Array.isArray(json)) {
                setActiveSubstances(json);
            }
            if (medicamentId) {
                refreshItem();
            }
        }, () => history.goBack());
    };
    const refreshItem = () => {
        requestData(`entity/medicament/${medicamentId}`, json => {
            if (typeof json === 'object') {
                if (json.id) {
                    setItem(json);
                } else {
                    setErrors(json);
                }
            } else if (typeof json === 'string') {
                setErrors({
                    warning: json
                });
            }
        }, () => history.goBack());
    };

    useEffect(() => {
        refresh();
    }, []);

    const handleChangeName = event => {
        setItem({
            ...item,
            name: event.target.value,
        })
    };

    const handleChangeActiveSubstance = event => {
        setItem({
            ...item,
            active_substance_id: event.target.value,
        })
    };

    const handleChangeManufacturer = event => {
        setItem({
            ...item,
            manufacturer_id: event.target.value,
        })
    };

    const handleChangePrice = event => {
        setItem({
            ...item,
            price: event.target.value,
        })
    };

    const submit = () => {
        const route = 'entity/medicament' + (medicamentId ? '/' + medicamentId : '');
        const method = medicamentId ? 'put' : 'post';
        const onSuccess = json => {
            if (typeof json === 'object') {
                setErrors(json);
            } else if (typeof json === 'string') {
                setErrors({
                    warning: json
                });
            } else {
                history.goBack();
            }
        };
        fetch(API_URL + route, {
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            method,
            headers: {
                'Authorization': 'Bearer ' + cookies.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(item),
        })
            .then(response => {
                if (response.status === 401) {
                    setCookie('token', '', { path: '/' });
                    return response.statusText;
                }
                if (response.status === 404) {
                    return response.statusText;
                }
                return response.json();
            })
            .then(onSuccess)
            .catch(() => {
                setErrors({
                    warning: 'Unknown error!',
                });
            })
        ;

    };

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">{medicamentId ? "Edit medicament" : "New medicament"}</div>

                        <div className="card-body">

                            {Object.values(errors).map(value => {
                                const error = Array.isArray(value) ? value[0] : value;
                                return <div key={value} className="alert alert-danger">{error}</div>
                            })}

                            <div className="form-group row">
                                <label htmlFor="name" className="col-md-4 col-form-label text-md-right">Name</label>
                                <div className="col-md-6">
                                    <input
                                        id="name"
                                        className="form-control"
                                        value={item.name}
                                        required
                                        autoFocus
                                        onChange={handleChangeName}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label htmlFor="active-substance" className="col-md-4 col-form-label text-md-right">
                                    Active substance
                                </label>
                                <div className="col-md-6">
                                    <select
                                        className="form-control"
                                        onChange={handleChangeActiveSubstance}
                                        id="active-substance"
                                        value={item.active_substance_id}
                                    >
                                        <option value={"0"}>-- select value --</option>
                                        {activeSubstances.map(el => (
                                            <option key={'as-' + el.id} value={'' + el.id}>{el.name}</option>
                                            )
                                        )}
                                    </select>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label htmlFor="manufacturer" className="col-md-4 col-form-label text-md-right">
                                    Manufacturer
                                </label>
                                <div className="col-md-6">
                                    <select
                                        className="form-control"
                                        onChange={handleChangeManufacturer}
                                        id="manufacturer"
                                        value={item.manufacturer_id}
                                    >
                                        <option value={"0"}>-- select value --</option>
                                        {manufacturers.map(el => (
                                            <option key={'m-' + el.id} value={'' + el.id}>{el.name}</option>
                                            )
                                        )}
                                    </select>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label htmlFor="price" className="col-md-4 col-form-label text-md-right">Price</label>
                                <div className="col-md-6">
                                    <input
                                        id="name"
                                        className="form-control"
                                        value={item.price}
                                        onChange={handleChangePrice}
                                    />
                                </div>
                            </div>

                            <div className="form-group row mb-0">
                                <div className="col-md-8 offset-md-4">
                                    <button onClick={submit} type="button" className="btn btn-primary">Submit</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
