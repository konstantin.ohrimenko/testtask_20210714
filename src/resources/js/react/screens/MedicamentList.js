import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {API_URL} from '../const';
import {useCookies} from 'react-cookie';
import {Link} from 'react-router-dom';

export default function MedicamentList() {
    const [itemList, setItemList] = useState([]);
    const [cookies, setCookie] = useCookies(['token']);
    const parseResponse = response => {
        if (response.status === 401) {
            setCookie('token', '', { path: '/' });
            return response.statusText;
        }
        return response.json();
    }
    const parseJson = json => {
        if (Array.isArray(json)) {
            setItemList(json);
        }
    }

    const refreshList = () => {
        fetch(API_URL + 'entity/medicament', {
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
                'Authorization': 'Bearer ' + cookies.token,
            },
        })
            .then(parseResponse)
            .then(parseJson)
            .catch(err => console.log('error', err))
        ;
    };
    const deleteItem = itemId => {
        fetch(API_URL + 'entity/medicament/' + itemId, {
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + cookies.token,
            },
        })
            .then(refreshList)
            .catch(refreshList)
        ;
    };

    useEffect(() => {
        refreshList();
    }, []);


    const views = itemList.map(item => (
        <tr key={'item-' + item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.price}</td>
            <td>
                <Link to={`/entity/medicament/${item.id}`}>Edit</Link>
            </td>
            <td>
                <button type="button" onClick={() => deleteItem(item.id)}>Delete</button>
            </td>
        </tr>
    ));
    if (!views.length) {
        views.push(
            <tr key={'item-empty'}>
                <td colSpan={5}>
                    <h4 className="text-center">No data to display</h4>
                </td>
            </tr>
        );
    }
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">
                            <div className="row">
                                <div className="col-8">Medicament list</div>
                                <div className="col-4 text-right">
                                    <Link to="/entity/medicament/create">Add</Link>
                                </div>
                            </div>
                        </div>

                        <div className="card-body">
                            <table className="table table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                {views}
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
