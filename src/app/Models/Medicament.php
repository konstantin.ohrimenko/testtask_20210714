<?php

namespace App\Models;

use App\Models\ActiveSubstance;
use App\Models\Manufacturer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicament extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'active_substance_id', 'manufacturer_id', 'price'];
    protected $hidden = ['created_at', 'updated_at'];
    
    public function activeSubstance()
    {
        return $this->hasOne(ActiveSubstance::class, 'id', 'active_substance_id');
    }
    
    public function manufacturer()
    {
        return $this->hasOne(Manufacturer::class, 'id', 'manufacturer_id');
    }
}
