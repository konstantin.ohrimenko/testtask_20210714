<?php

namespace App\Http\Controllers;

use App\Models\Medicament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MedicamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Medicament::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Medicament $medicament)
    {
        return $medicament;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Medicament $medicament)
    {
        $validator = Validator::make($request->all(), $this->getRules());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $medicament->fill($request->all());
        $medicament->save();
        return $medicament->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medicament = Medicament::where('id', $id)->first();
        if ($medicament) {
            return $medicament;
        }
        return response()->json(['error' => 'Not found!'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $medicament = Medicament::where('id', $id)->first();
        if (!$medicament) {
            return response()->json(['error' => 'Not found!'], 404);
        }
        $validator = Validator::make($request->all(), $this->getRules());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $medicament->fill($request->all());
        $medicament->save();
        return $medicament->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medicament = Medicament::where('id', $id)->first();
        if ($medicament) {
            $medicament->delete();
        } else {
            return response()->json(['error' => 'Not found!'], 404);
        }
    }
    
    private function getRules()
    {
        return [
            'name' => 'required|string|min:3|max:128',
            'price' => 'required|numeric|min:0.01',
            'active_substance_id' => 'required|exists:active_substances,id',
            'manufacturer_id' => 'required|exists:manufacturers,id'
        ];
    }
}
