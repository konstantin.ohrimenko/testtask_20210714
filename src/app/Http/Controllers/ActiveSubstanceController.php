<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActiveSubstance;

class ActiveSubstanceController extends Controller
{
    public function __invoke()
    {
        return ActiveSubstance::get();
    }
}
