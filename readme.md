<h1>Test Task</h1>

<h2>Install</h2>

<h3>Linux</h3>
<ol>
  <li>
    Clone repository
    <pre>git clone https://gitlab.com/konstantin.ohrimenko/testtask_20210714.git</pre>
  </li>
  <li>
    Start docker image
    <pre>cd testtask_20210714
    docker-compose buid
    docker up -d</pre>
  </li>
  <li>
    Install dependencies
    <pre>sudo docker exec -it test_task_php /bin/bash
    composer install
    npm install</pre>
  </li>
  <li>
    Add mock data
    <pre>php artisan db:seed</pre>
  </li>
  <li>
    Start project
    <pre>npm run dev</pre>
  </li>
  <li>
    Open <a href="http://localhost:13080" target="_blank">project home page</a>
  </li>
</ol>


<h2>API documentation</h2>
<a href="https://app.swaggerhub.com/apis-docs/konstantin.ohrimenko/TestTask/1.0.1" target="_blank">TestTask API documentation on app.swaggerhub.com</a>


